# Config Sync

This repo contains my personal configurations that I want to sync across my machines.

## Requirements

You need to have [Poetry](https://python-poetry.org/) installed.
Best way how to do it is to use [pipx](https://pipxproject.github.io/pipx/installation/).
I prefer to install pipx as system package eg. by `sudo apt install pipx`.

## Initialization
Simply run `poetry shell`. It will initialize virtual environment and install dependencies.

After that you can use `ansible` as usual.

## Execution
```
ansible-playbook -l localhost sync-repos.yaml
```
